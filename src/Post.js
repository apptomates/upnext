import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {connect} from 'react-redux';

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      delete:false
    };

    this.edittoggle = this.edittoggle.bind(this);
    this.deletetoggle = this.deletetoggle.bind(this);

  }

  edittoggle() {
    this.setState({
      edit: !this.state.edit
    });
  }

 deletetoggle() {
    this.setState({
      delete: !this.state.delete
    });
  }

  handleEdit = (e) => {
    e.preventDefault();
    const newTitle = this.getTitle.value;
    const newMessage = this.getMessage.value;
    const data = {
      newTitle,
      newMessage
    }
    this.props.dispatch({ type: 'UPDATE', id: this.props.post.id, data: data })
  }

  render() {
    const editCloseBtn = <button className="close" onClick={this.edittoggle}>&times;</button>;
    const deleteCloseBtn = <button className="close" onClick={this.deletetoggle}>&times;</button>;

  return (
    <div>
      <h2>{this.props.post.title}</h2>
      <p>{this.props.post.message}</p>
      <button
       onClick={this.edittoggle}>
       Edit</button>
      <button onClick={this.deletetoggle} >
      Delete</button>

       {/* Delete Post Modal*/}
      <Modal isOpen={this.state.delete} toggle={this.deletetoggle} className={this.props.className}>
      <ModalHeader toggle={this.deletetoggle} close={deleteCloseBtn}>Delete</ModalHeader>
      <ModalBody>
        Are u Sure want to delete?
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={()=>this.props.dispatch({type:'DELETE_POST',id:this.props.post.id})}>Delete</Button> 
        <Button color="secondary" onClick={this.deletetoggle}>Cancel</Button>
      </ModalFooter>
      </Modal>

      {/* Update Post Modal*/}
      <Modal isOpen={this.state.edit} toggle={this.edittoggle} className={this.props.className}>
      <ModalHeader toggle={this.edittoggle} close={editCloseBtn}>Edit</ModalHeader>
      <ModalBody>
                <form onSubmit={this.handleEdit}>
                  <input required type="text" ref={(input) => this.getTitle = input}
                  defaultValue={this.props.post.title} placeholder="Enter Post Title" /><br /><br />
                  <textarea required rows="5" ref={(input) => this.getMessage = input}
                  defaultValue={this.props.post.message} cols="28" placeholder="Enter Post" /><br /><br />
                  <Button color="primary" type="submit">Update</Button> 
                  <Button color="secondary" onClick={this.edittoggle}>Cancel</Button>
                </form>
        </ModalBody>
      </Modal>
</div>
  );
 }
}
export default connect()(Post);