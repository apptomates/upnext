import React from 'react';

const Post = React.lazy(() => import('./PostForm'));
const Dashboard = React.lazy(() => import('./components/Content'));
const AllPost = React.lazy(() => import('./AllPost'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Dashboard',  component: Dashboard},
  { path: '/post', name: 'Post',  component: Post },
  { path: '/allpost', name: 'AllPost',  component: AllPost },
];

export default routes;