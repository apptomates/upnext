// App.js

import React, { Component,Suspense } from 'react';
import Header from './components/Header';
import SideBar from './components/SideBar';
import RSideBar from './components/RSideBar';
import Footer from './components/Footer';
import { BrowserRouter as Router,Redirect, Route, Switch, HashRouter } from 'react-router-dom';

// sidebar nav config
import routes from './routes';


class App extends Component {

  loading = () => <div id="preloader">
          <div className="canvas">
              <img src="assets/img/logo.png" alt="logo" className="loader-logo"></img>
              <div className="spinner"></div>   
          </div>
      </div>
 
  render() {
    return ( 
      <HashRouter>
        <div className="page">
          <Header />
          <div className="page-content d-flex align-items-stretch">
                <SideBar />
                <div className="content-inner">

               <Suspense fallback={this.loading()}>
               <Switch>
                    {routes.map((route, index) => (
                   
                      <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.component}
                      />
                   
                  ))}
                   </Switch>
                  </Suspense>
                  <Footer />
                  <a href="#" className="go-top"><i className="la la-arrow-up"></i></a>
                  <RSideBar />
                </div>
          </div>
        </div>
        </HashRouter>
    );
  }
}

export default App;