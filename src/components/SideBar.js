// SideBar.js

import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link } from 'react-router-dom';
import items from '../nav';
export default class SideBar extends Component {
    render(){
        return (

                 <div className="default-sidebar">
                            <nav className="side-navbar box-scroll sidebar-scroll">         
                                    <ul className="list-unstyled">
        
                                     {/*  <li>
                                     
                                           {items.map((item, index) => (
                 
                                              <Link to={item.url}>{item.name}</Link>
                  
                                           ))}
                                         
                                        </li> */} 
                                        <li>
                                        <Link to="/">Home</Link>
                                        </li>
                                        <li>
                                          <Link to="/post">Post</Link>
                                        </li>
                                        <li>
                                          <Link to="/allpost">All Post</Link>
                                        </li>

                                     </ul>
                            </nav>
                 </div>
      
        )
    }
}