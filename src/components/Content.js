// Content.js

import React, {Component} from 'react';

export default class Content extends Component {
    render(){
        return (
            <div className="container-fluid">
       
            <div className="row">
                <div className="page-header">
                    <div className="d-flex align-items-center">
                        <h2 className="page-header-title">Dashboard</h2>
                    </div>
                </div>
            </div>
            <div className="row flex-row">
                <div className="col-xl-12">
                   
                    <div className="widget widget-07 has-shadow">
                    
                        <div className="widget-header bordered d-flex align-items-center">
                            <h2>Data Table</h2>
                            <div className="widget-options">
                                <div className="btn-group" role="group">
                                    <button type="button" className="btn btn-primary ripple">Week</button>
                                    <button type="button" className="btn btn-primary ripple">Month</button>
                                </div>
                            </div>
                        </div>
                       
                        <div className="widget-body">
                            <div className="table-responsive table-scroll padding-right-10" style={{ height: '520px'}}>
                                <table className="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div className="styled-checkbox mt-2">
                                                    <input type="checkbox" name="check-all" id="check-all"></input>
                                                    <label htmlFor="check-all"></label>
                                                </div>
                                            </th>
                                            <th>Order ID</th>
                                            <th>Customer Name</th>
                                            <th>Country</th>
                                            <th>Ship Date</th>
                                            <th><span style={{width: '100px'}}>Status</span></th>
                                            <th>Order Total</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style={{width: '5%'}}>
                                                <div className="styled-checkbox mt-2">
                                                    <input type="checkbox" name="cb1" id="cb1"></input>
                                                    <label htmlFor="cb1"></label>
                                                </div>
                                            </td>
                                            <td><span className="text-primary">054-01-FR</span></td>
                                            <td>Lori Baker</td>
                                            <td>US</td>
                                            <td>10/21/2017</td>
                                            <td><span style={{width:'100px'}}><span className="badge-text badge-text-small info">Paid</span></span></td>
                                            <td>$139.45</td>
                                            <td className="td-actions">
                                                <a href="#"><i className="la la-edit edit"></i></a>
                                                <a href="#"><i className="la la-close delete"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style={{ width:'5%' }}>
                                                <div className="styled-checkbox mt-2">
                                                    <input type="checkbox" name="cb2" id="cb2"></input>
                                                    <label htmlFor="cb2"></label>
                                                </div>
                                            </td>
                                            <td><span className="text-primary">021-12-US</span></td>
                                            <td>Lawrence CrawhtmlFord</td>
                                            <td>FR</td>
                                            <td>10/21/2017</td>
                                            <td><span style={{ width:'100px' }}><span className="badge-text badge-text-small info">Paid</span></span></td>
                                            <td>$189.00</td>
                                            <td className="td-actions">
                                                <a href="#"><i className="la la-edit edit"></i></a>
                                                <a href="#"><i className="la la-close delete"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style={{width:'5%'}}>
                                                <div className="styled-checkbox mt-2">
                                                    <input type="checkbox" name="cb3" id="cb3"></input>
                                                    <label htmlFor="cb3"></label>
                                                </div>
                                            </td>
                                            <td><span className="text-primary">189-01-RU</span></td>
                                            <td>Samuel Walker</td>
                                            <td>AU</td>
                                            <td>08/21/2017</td>
                                            <td><span style={{ width:'100px' }}><span className="badge-text badge-text-small danger">Failed</span></span></td>
                                            <td>$107.55</td>
                                            <td className="td-actions">
                                                <a href="#"><i className="la la-edit edit"></i></a>
                                                <a href="#"><i className="la la-close delete"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style={{ width:'5%' }}>
                                                <div className="styled-checkbox mt-2">
                                                    <input type="checkbox" name="cb4" id="cb4"></input>
                                                    <label htmlFor="cb4"></label>
                                                </div>
                                            </td>
                                            <td><span className="text-primary">092-06-FR</span></td>
                                            <td>Angela Walters</td>
                                            <td>US</td>
                                            <td>08/21/2017</td>
                                            <td><span style={{ width:'100px' }}><span className="badge-text badge-text-small success">Delivered</span></span></td>
                                            <td>$129.85</td>
                                            <td className="td-actions">
                                                <a href="#"><i className="la la-edit edit"></i></a>
                                                <a href="#"><i className="la la-close delete"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style={{ width:'5%' }}>
                                                <div className="styled-checkbox mt-2">
                                                    <input type="checkbox" name="cb5" id="cb5"></input>
                                                    <label htmlFor="cb5"></label>
                                                </div>
                                            </td>
                                            <td><span className="text-primary">021-09-US</span></td>
                                            <td>Ryan Hanson</td>
                                            <td>ES</td>
                                            <td>07/21/2017</td>
                                            <td><span style={{ width:'100px'}}><span className="badge-text badge-text-small info">Paid</span></span></td>
                                            <td>$199.95</td>
                                            <td className="td-actions">
                                                <a href="#"><i className="la la-edit edit"></i></a>
                                                <a href="#"><i className="la la-close delete"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                        <div className="widget-footer d-flex align-items-center">
                            <div className="mr-auto p-2">
                                <span className="display-items">Showing 1-30 / 150 Results</span>
                            </div>
                            <div className="p-2">
                                <nav aria-label="...">
                                    <ul className="pagination justify-content-end">
                                        <li className="page-item disabled">
                                            <span className="page-link"><i className="ion-chevron-left"></i></span>
                                        </li>
                                        <li className="page-item"><a className="page-link" href="#">1</a></li>
                                        <li className="page-item active">
                                            <span className="page-link">2<span className="sr-only">(current)</span></span>
                                        </li>
                                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        <li className="page-item">
                                            <a className="page-link" href="#"><i className="ion-chevron-right"></i></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                      
                    </div>
                  
                </div>
            </div>
           
        </div>
     
        )
    }
}